#!/bin/bash

set -e

otaFile="$1"

if [ -z "$otaFile" ] || [ ! -f "$otaFile" ]; then
    echo "Usage: $0 <path-to-OTA-file>"
    exit 1
fi

if [ ! -d "common" ]; then
    echo "Please run this script from the root of the repository!"
    exit 1
fi

copyApk() {
    destAppName="$1"
    destFolder="$2"
    if [ -d /mnt/system ]; then
        sourceAppFolder="/mnt/system/system"
    elif [ -d /mnt/product ]; then
        sourceAppFolder="/mnt/product"
    else
        sourceAppFolder="/mnt/system_ext"
    fi
    cp "$sourceAppFolder/app/$destAppName"*/*.apk "$destFolder/$destAppName/$destAppName.apk" 2>/dev/null || true
    cp "$sourceAppFolder/priv-app/$destAppName"*/*.apk "$destFolder/$destAppName/$destAppName.apk" 2>/dev/null || true
    if [[ "$destAppName" == "PrebuiltGmsCore" ]]; then
        cp "$sourceAppFolder/priv-app/PrebuiltGmsCore"*/m/independent/AndroidPlatformServices.apk "$destFolder/AndroidPlatformServices/AndroidPlatformServices.apk"
    elif [[ "$destAppName" == "Chrome" ]] || [[ "$destAppName" == "TrichromeLibrary" ]] || [[ "$destAppName" == "WebViewGoogle" ]]; then
        cp "$sourceAppFolder/app/$destAppName"*/*.apk.gz "$destFolder/$destAppName/$destAppName.apk.gz"
        cp "$sourceAppFolder/app/${destAppName}-Stub/"*.apk "$destFolder/${destAppName}-Stub/${destAppName}-Stub.apk"
        rm "$destFolder/$destAppName/$destAppName.apk"
    fi
}

rm -rf /tmp/ota
sudo umount /mnt/system 2>/dev/null || true
sudo umount /mnt/product 2>/dev/null || true
sudo umount /mnt/system_ext 2>/dev/null || true

echo "Extracting payload file from $otaFile..."
unzip -qq -o "$otaFile" -d /tmp/ota

echo "Extracting partitions from payload file..."
payload_dumper --partitions product,system,system_ext --out /tmp/ota/payload /tmp/ota/payload.bin &>/dev/null

echo "Copying proprietary files from /system..."
sudo mkdir -p /mnt/system
sudo mount -t ext4 -o ro /tmp/ota/payload/system.img /mnt/system
copyApk GoogleExtShared ./common/proprietary/app
copyApk GooglePrintRecommendationService ./common/proprietary/app
copyApk GooglePackageInstaller ./common/proprietary/priv-app
sudo umount /mnt/system
sudo rm -rf /mnt/system

echo "Copying proprietary files from /product..."
sudo mkdir -p /mnt/product
sudo mount -t ext4 -o ro /tmp/ota/payload/product.img /mnt/product
copyApk arcore ./common/proprietary/product/app
copyApk CalculatorGooglePrebuilt ./common/proprietary/product/app
copyApk CalendarGooglePrebuilt ./common/proprietary/product/app
copyApk Chrome ./common/proprietary/product/app
copyApk GoogleContacts ./common/proprietary/product/app
copyApk GoogleTTS ./common/proprietary/product/app
copyApk LatinIMEGooglePrebuilt ./common/proprietary/product/app
copyApk LocationHistoryPrebuilt ./common/proprietary/product/app
copyApk Maps ./common/proprietary/product/app
copyApk Photos ./common/proprietary/product/app
copyApk PrebuiltDeskClockGoogle ./common/proprietary/product/app
copyApk PrebuiltGmail ./common/proprietary/product/app
copyApk talkback ./common/proprietary/product/app
copyApk TrichromeLibrary ./common/proprietary/product/app
copyApk WebViewGoogle ./common/proprietary/product/app
copyApk AndroidAutoStubPrebuilt ./common/proprietary/product/priv-app
copyApk ConfigUpdater ./common/proprietary/product/priv-app
copyApk GoogleDialer ./common/proprietary/product/priv-app
copyApk GoogleOneTimeInitializer ./common/proprietary/product/priv-app
copyApk GoogleRestorePrebuilt ./common/proprietary/product/priv-app
copyApk PartnerSetupPrebuilt ./common/proprietary/product/priv-app
copyApk Phonesky ./common/proprietary/product/priv-app
copyApk PrebuiltBugle ./common/proprietary/product/priv-app
copyApk PrebuiltGmsCore ./common/proprietary/product/priv-app
copyApk SetupWizardPrebuilt ./common/proprietary/product/priv-app
copyApk Velvet ./common/proprietary/product/priv-app
copyApk WellbeingPrebuilt ./common/proprietary/product/priv-app
sudo umount /mnt/product
sudo rm -rf /mnt/product

echo "Copying proprietary files from /system_ext..."
sudo mkdir -p /mnt/system_ext
sudo mount -t ext4 -o ro /tmp/ota/payload/system_ext.img /mnt/system_ext
copyApk GoogleFeedback ./common/proprietary/system_ext/priv-app
copyApk GoogleServicesFramework ./common/proprietary/system_ext/priv-app
sudo umount /mnt/system_ext
sudo rm -rf /mnt/system_ext

echo "Cleaning up..."
rm -rf /tmp/ota

echo "Finished!"
