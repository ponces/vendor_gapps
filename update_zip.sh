#!/bin/bash

set -e

zipFile="$1"

if [ -z "$zipFile" ] || [ ! -f "$zipFile" ]; then
    echo "Usage: $0 <path-to-ZIP-file>"
    exit 1
fi

if [ ! -d "common" ]; then
    echo "Please run this script from the root of the repository!"
    exit 1
fi

copyApk() {
    sourceAppName="$1"
    destAppName="$2"
    destFolder="$3"
    sourceAppFolder="/tmp/zip/partner_gms/apps"
    cp "$sourceAppFolder/$sourceAppName/${sourceAppName}.apk" "$destFolder/$destAppName/$destAppName.apk" 2>/dev/null || true
    cp "$sourceAppFolder/$sourceAppName/${sourceAppName}Stub.apk" "$destFolder/$destAppName/$destAppName.apk" 2>/dev/null || true
    cp "$sourceAppFolder/$sourceAppName/${sourceAppName}_arm64_xxhdpi.apk" "$destFolder/$destAppName/$destAppName.apk" 2>/dev/null || true
    cp "$sourceAppFolder/$sourceAppName/${sourceAppName}_arm64.apk" "$destFolder/$destAppName/$destAppName.apk" 2>/dev/null || true
    compressApk "$destAppName" "$destFolder"
    if [[ "$destAppName" == "Chrome" ]]; then
        cp "$sourceAppFolder/Chrome/TrichromeLibrary_arm64.apk" "$destFolder/TrichromeLibrary/TrichromeLibrary.apk"
        compressApk TrichromeLibrary "$destFolder"
    fi
}

compressApk() {
    destAppName="$1"
    destFolder="$2"
    if [[ "$destAppName" == "Chrome" ]] || [[ "$destAppName" == "TrichromeLibrary" ]] || [[ "$destAppName" == "WebViewGoogle" ]]; then
        if [ -f "$destFolder/$destAppName/$destAppName.apk" ]; then
            gzip -c "$destFolder/$destAppName/$destAppName.apk" > "$destFolder/$destAppName/$destAppName.apk.gz"
            rm "$destFolder/$destAppName/$destAppName.apk"
        fi
    fi
}

rm -rf /tmp/zip

echo "Extracting $zipFile..."
unzip -qq "$zipFile" -d /tmp/zip

echo "Copying proprietary files from /system..."
copyApk GoogleExtShared GoogleExtShared ./common/proprietary/app
copyApk GooglePrintRecommendationService GooglePrintRecommendationService ./common/proprietary/app
copyApk GooglePackageInstaller GooglePackageInstaller ./common/proprietary/priv-app

echo "Copying proprietary files from /product..."
copyApk arcore arcore ./common/proprietary/product/app
copyApk CalculatorGoogle CalculatorGooglePrebuilt ./common/proprietary/product/app
copyApk CalendarGoogle CalendarGooglePrebuilt ./common/proprietary/product/app
copyApk Chrome Chrome ./common/proprietary/product/app
copyApk GoogleContacts GoogleContacts ./common/proprietary/product/app
copyApk SpeechServicesByGoogle GoogleTTS ./common/proprietary/product/app
copyApk LatinImeGoogle LatinIMEGooglePrebuilt ./common/proprietary/product/app
copyApk GoogleLocationHistory LocationHistoryPrebuilt ./common/proprietary/product/app
copyApk Maps Maps ./common/proprietary/product/app
copyApk Photos Photos ./common/proprietary/product/app
copyApk DeskClockGoogle PrebuiltDeskClockGoogle ./common/proprietary/product/app
copyApk Gmail2 PrebuiltGmail ./common/proprietary/product/app
copyApk talkback talkback ./common/proprietary/product/app
copyApk WebViewGoogle WebViewGoogle ./common/proprietary/product/app
copyApk AndroidAuto AndroidAutoStubPrebuilt ./common/proprietary/product/priv-app
copyApk AndroidPlatformServices AndroidPlatformServices ./common/proprietary/product/priv-app
copyApk ConfigUpdater ConfigUpdater ./common/proprietary/product/priv-app
copyApk GmsCore PrebuiltGmsCore ./common/proprietary/product/priv-app
copyApk GoogleDialer GoogleDialer ./common/proprietary/product/priv-app
copyApk GooglePartnerSetup PartnerSetupPrebuilt ./common/proprietary/product/priv-app
copyApk GoogleOneTimeInitializer GoogleOneTimeInitializer ./common/proprietary/product/priv-app
copyApk GoogleRestore GoogleRestorePrebuilt ./common/proprietary/product/priv-app
copyApk Messages PrebuiltBugle ./common/proprietary/product/priv-app
copyApk Phonesky Phonesky ./common/proprietary/product/priv-app
copyApk SetupWizard SetupWizardPrebuilt ./common/proprietary/product/priv-app
copyApk Velvet Velvet ./common/proprietary/product/priv-app
copyApk Wellbeing WellbeingPrebuilt ./common/proprietary/product/priv-app

echo "Copying proprietary files from /system_ext..."
copyApk GoogleFeedback GoogleFeedback ./common/proprietary/system_ext/priv-app
copyApk GoogleServicesFramework GoogleServicesFramework ./common/proprietary/system_ext/priv-app

echo "Copying configuration files..."
sourceFolder="/tmp/zip/partner_gms"
cp "$sourceFolder"/apps/GoogleDialer/com.google.android.dialer.support.jar ./common/proprietary/product/framework
cp "$sourceFolder"/apps/GoogleDialer/com.google.android.dialer.support.xml ./common/proprietary/product/etc/permissions
cp "$sourceFolder"/etc/default-permissions/default-permissions-google.xml ./common/proprietary/product/etc/default-permissions
cp "$sourceFolder"/etc/permissions/privapp-permissions-google-system.xml ./common/proprietary/etc/permissions
cp "$sourceFolder"/etc/permissions/privapp-permissions-google-comms-suite.xml ./common/proprietary/product/etc/permissions
cp "$sourceFolder"/etc/permissions/privapp-permissions-google-product.xml ./common/proprietary/product/etc/permissions
cp "$sourceFolder"/etc/permissions/privapp-permissions-google-system_ext.xml ./common/proprietary/system_ext/etc/permissions
cp "$sourceFolder"/etc/permissions/split-permissions-google.xml ./common/proprietary/product/etc/permissions
cp "$sourceFolder"/etc/preferred-apps/gms/google.xml ./common/proprietary/product/etc/preferred-apps
cp "$sourceFolder"/etc/security/gms_fsverity_cert.der ./common/proprietary/product/etc/security/fsverity
cp "$sourceFolder"/etc/security/play_store_fsi_cert.der ./common/proprietary/product/etc/security/fsverity
cp "$sourceFolder"/etc/sysconfig/d2d_cable_migration_feature.xml ./common/proprietary/product/etc/sysconfig
cp "$sourceFolder"/etc/sysconfig/google-hiddenapi-package-allowlist.xml ./common/proprietary/etc/sysconfig
cp "$sourceFolder"/etc/sysconfig/wellbeing.xml ./common/proprietary/product/etc/sysconfig
cp "$sourceFolder"/etc/sysconfig/gms/google.xml ./common/proprietary/product/etc/sysconfig
cp "$sourceFolder"/overlay/GmsConfigOverlayCommon/res/values/vendor_cross_profile_apps.xml ./common/rro_overlays/GmsConfigOverlay/res/values
cp "$sourceFolder"/overlay/GmsConfigOverlayCommon/res/values/vendor_required_apps_managed_device.xml ./common/rro_overlays/GmsConfigOverlay/res/values
cp "$sourceFolder"/overlay/GmsConfigOverlayCommon/res/values/vendor_required_apps_managed_profile.xml ./common/rro_overlays/GmsConfigOverlay/res/values
cp "$sourceFolder"/overlay/GmsConfigOverlayCommon/res/values/vendor_required_apps_managed_user.xml ./common/rro_overlays/GmsConfigOverlay/res/values
cp "$sourceFolder"/overlay/GmsConfigOverlayCommon/res/xml/config_webview_packages.xml ./common/rro_overlays/GmsConfigOverlay/res/xml
cp "$sourceFolder"/overlay/gms_overlay_comms/packages/services/Telecomm/res/values/config.xml ./common/rro_overlays/TelecommConfigOverlay/res/values

echo "Cleaning up..."
rm -rf /tmp/zip

echo "Finished!"
